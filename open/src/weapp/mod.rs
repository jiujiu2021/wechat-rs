//! copyright © ecdata.cn 2022 - present
//! 基于开放平台的微信小程序处理

mod auth;
pub use auth::*;

mod category;
pub use category::*;

mod code;
pub use code::*;

mod tester;
pub use tester::*;

mod domain;
pub use domain::Domain;
